import string
import random
import twitter

# These are retrieved from your twitter app
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_TOKEN = ''
ACCESS_TOKEN_SECRET = ''

CHR_LIST = list(string.ascii_uppercase + string.digits)


def create_auth_code():
    auth_code = ''
    for i in xrange(0, 9):
        auth_code += random.sample(CHR_LIST, 1)[0]

    print "Generated new auth code: " + auth_code
    return auth_code


def post_to_twitter(api, auth_code):
    status = '::authdc ' + auth_code
    print "Posting to Twitter..."
    api.PostUpdate(status)


def main():
    api = twitter.Api(consumer_key=CONSUMER_KEY,
                      consumer_secret=CONSUMER_SECRET,
                      access_token_key=ACCESS_TOKEN,
                      access_token_secret=ACCESS_TOKEN_SECRET)

    auth_code = create_auth_code()
    post_to_twitter(api, auth_code)

if __name__ == '__main__':
    main()

Twitter Auth Code Generator
===========================

Setup
-----

This script requires python 2.7 and the python twitter wrapper.

To install the python twitter wrapper [download it here](https://code.google.com/p/python-twitter/)
and follow the instructions on the site.

You must also setup a twitter app.

Go to [apps.twitter.com](http://apps.twitter.com) and create a new app.

Fill out the form on the next page:

![twitter app form](http://i.imgur.com/9q1NO8s.png)

Then you'll be able to retrieve your keys and access tokens from the
keys and access tokens tab.

Your consumer keys will look like this:

![consumer keys](http://i.imgur.com/S3tGixg.png)

Add those to the twitter_auth_code_bot.py

Then go to the bottom of the page and find the button to generate your access
tokens.

After generating your access tokens you'll see something like this:

![access tokens](http://i.imgur.com/kDuMrAk.png)

Add those to the twitter_auth_code_bot.py
